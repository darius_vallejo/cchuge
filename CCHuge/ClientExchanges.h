//
//  ClientExchanges.h
//  CCHuge
//
//  Created by DariusVallejo on 16/12/15.
//  Copyright © 2015 DarioVallejo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClientExchanges : NSObject
@property (strong, nonatomic) NSArray *arrayCtype;
- (void)getCurrencyApi: (void (^)(NSDictionary *completion))completion;
@end
