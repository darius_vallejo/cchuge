//
//  ExchangesModel.h
//  CCHuge
//
//  Created by DariusVallejo on 16/12/15.
//  Copyright © 2015 DarioVallejo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExchangesModel : NSObject
@property (readonly, nonatomic) NSNumber *rate;
@property (readonly, nonatomic) NSString *idCurrency;
@end
