//
//  ClientExchanges.m
//  CCHuge
//
//  Created by DariusVallejo on 16/12/15.
//  Copyright © 2015 DarioVallejo. All rights reserved.
//

#import "ClientExchanges.h"
#import <AFNetworking.h>


@implementation ClientExchanges
@synthesize arrayCtype;

- (instancetype)init{
    if (self = [super init]) {
        arrayCtype = [NSArray arrayWithContentsOfFile:
                       [[NSBundle mainBundle] pathForResource:@"CoinsType"
                                                       ofType:@"plist"]];
    }
    return self;
}



- (void)getCurrencyApi: (void (^)(NSDictionary *completion))completion{
    NSString *url = @"http://api.fixer.io/latest?base=USD";
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]init];
    [manager GET:url parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
//        NSLog(@"response %@",downloadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject[@"rates"]);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"response %@",error);
    }];

}


@end
