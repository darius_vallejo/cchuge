//
//  main.m
//  CCHuge
//
//  Created by DariusVallejo on 16/12/15.
//  Copyright © 2015 DarioVallejo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
