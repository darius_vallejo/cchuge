//
//  ViewController.h
//  CCHuge
//
//  Created by DariusVallejo on 16/12/15.
//  Copyright © 2015 DarioVallejo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PNBarChart.h"
#import "ClientExchanges.h"

@interface ViewController : UIViewController

@property (weak, nonatomic)     IBOutlet UIActivityIndicatorView *activity;
@property (strong, nonatomic)   IBOutlet UIButton *btnConvert;
@property (weak, nonatomic)     IBOutlet UITextField *txtFieldValue;
@property (strong, nonatomic)   PNBarChart * barChart;
- (NSInteger)getTotalFromString: (NSString*)str andFloat:(float)vlrFloat;
- (BOOL) validateIfIsNumber: (NSString*)vlrText;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constrains;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contrains1;

@end

