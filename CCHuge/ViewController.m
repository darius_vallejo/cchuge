//
//  ViewController.m
//  CCHuge
//
//  Created by DariusVallejo on 16/12/15.
//  Copyright © 2015 DarioVallejo. All rights reserved.
//

#import "ViewController.h"
#import "ClientExchanges.h"


@interface ViewController ()<UITextFieldDelegate>

@end

@implementation ViewController{
    ClientExchanges *clientExchanges;
    CGFloat         vlrRate;
    NSDictionary    *dicRates;
    NSMutableArray  *arrayObjsY;
}

@synthesize btnConvert, txtFieldValue, barChart;

- (void)viewDidLoad {
    [super viewDidLoad];
    [_activity startAnimating];
    txtFieldValue.keyboardType = UIKeyboardTypeNumberPad;
    
    //Load Data
    clientExchanges = [ClientExchanges new];
    NSString *kRate = clientExchanges.arrayCtype[0][@"id"];
    [clientExchanges getCurrencyApi:^(NSDictionary *completion) {
        if (completion == nil) return;
        vlrRate = [completion[kRate] floatValue];
        dicRates = completion;
        btnConvert.tag = 1;
        [_activity stopAnimating];
    }];
    
    //Load the graph
    barChart = [[PNBarChart alloc] initWithFrame:(CGRect){ 5, txtFieldValue.frame.size.height + txtFieldValue.frame.origin.y, self.view.frame.size.width, txtFieldValue.superview.frame.size.height * .4}];
    [txtFieldValue.superview addSubview:barChart];
    
    arrayObjsY = [NSMutableArray array];
    for ( int i = 0; i<= clientExchanges.arrayCtype.count -1; i++){
        [arrayObjsY addObject:@0];
    }
    [self setCharBar];
    
    //Dismiss with touch
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
}

- (void)dismissKeyboard{
    [txtFieldValue resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (IBAction)actionExchangeCurrency:(UIButton*)sender {
    if (sender.tag == 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Problem!" message:@"You need internet connection" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self viewDidLoad];
        }];
        [alert addAction:okButton];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    [txtFieldValue resignFirstResponder];

    if (![self validateIfIsNumber:txtFieldValue.text])return;
    
    arrayObjsY = [NSMutableArray array];
    for (NSDictionary *row in clientExchanges.arrayCtype){
        NSString *kRate = row[@"id"];
        vlrRate = [dicRates[kRate] floatValue];
//        NSNumberFormatter *numFormatter = [[NSNumberFormatter alloc]init];
//        NSNumber *vlrFromUser = [numFormatter numberFromString:txtFieldValue.text];
//        NSInteger total = vlrRate*[vlrFromUser floatValue];
        [arrayObjsY addObject:
         [NSNumber numberWithInteger:
          [self getTotalFromString:txtFieldValue.text andFloat:vlrRate]]];
    }
    
    [self setCharBar];
}

- (NSInteger)getTotalFromString: (NSString*)str andFloat:(float)vlrFloat{
    NSNumberFormatter *numFormatter = [[NSNumberFormatter alloc]init];
    NSNumber *vlrFromUser = [numFormatter numberFromString:str];
    NSInteger total = vlrFloat*[vlrFromUser floatValue];
    return total;
}

- (void) setCharBar{
    NSMutableArray *arrayObjX = [NSMutableArray array];
    for (NSDictionary *dic in clientExchanges.arrayCtype) {
        [arrayObjX addObject:dic[@"name"]];
    }
    [barChart setXLabels:arrayObjX];
    [barChart setYValues:arrayObjsY];
    [barChart strokeChart];
}



- (BOOL) validateIfIsNumber: (NSString*)vlrText{
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if (([vlrText rangeOfCharacterFromSet:notDigits].location != NSNotFound) ||
        [vlrText isEqualToString: @""]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Problem!" message:@"Enter a number" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [txtFieldValue becomeFirstResponder];
            [txtFieldValue setText:@""];
        }];
        [alert addAction:okButton];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    return YES;
}


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"constant %f",_constrains.constant);
    self.constrains.constant += self.view.frame.size.height*.5;
    self.contrains1.constant += self.view.frame.size.height*.4;
    [btnConvert setNeedsUpdateConstraints];
    [UIView animateWithDuration:.3 animations:^{
        [btnConvert layoutIfNeeded];
        barChart.alpha = 0;
    NSLog(@"constant %f",_constrains.constant);
    }];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    self.constrains.constant -= self.view.frame.size.height*.5;
    self.contrains1.constant -= self.view.frame.size.height*.4;
    [btnConvert setNeedsUpdateConstraints];
    [UIView animateWithDuration:.3 animations:^{
        [btnConvert layoutIfNeeded];
        barChart.alpha = 1;
    }];
}

@end
