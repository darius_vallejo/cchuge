//
//  ViewControllerTest.m
//  CCHuge
//
//  Created by DariusVallejo on 17/12/15.
//  Copyright © 2015 DarioVallejo. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewControllerTest : XCTestCase
@property (nonatomic, strong) ViewController *viewController;
@end

@implementation ViewControllerTest

- (void)setUp {
    [super setUp];
    UIApplication *application = [UIApplication sharedApplication];
    AppDelegate *appDelegate = [application delegate];
    UIWindow *window = [appDelegate window];
    self.viewController = (ViewController *)[window rootViewController];
}

- (void)tearDown {
    self.viewController = nil;
    [super tearDown];
}


- (void)testViewController {
    XCTAssert([self.viewController view] ,@"The ViewController's UIView is nil");
}

- (void)testIBOutletActivityIndicator_activity{
    XCTAssertNotNil(self.viewController.activity, @"The property activity is nil");
}

- (void)testIBOutletUIButton_btnConvert{
    XCTAssertNotNil(self.viewController.btnConvert, @"The \"btnConvert\" property is nil");
}

- (void)testIBOutletUITextField_txtFieldValue{
    XCTAssertNotNil(self.viewController.txtFieldValue, @"The \"txtFieldValue\" property is nil");
}

- (void)testPropertyAndLibraryPNBarChar_barChart{

    XCTAssertNotNil(self.viewController.barChart, @"The \"barChart\" property is nil");
    
}

///Functions

-(void)testMultiplicationWithString{
    XCTAssert([self.viewController getTotalFromString:@"8" andFloat:2] == 16, @"the return value is wrong");
}

-(void)testValiteIfIsNumberWithString{
    XCTAssertFalse([self.viewController validateIfIsNumber:@"qwe"], @"The method has a problem");
}

@end
